<?php
	
	class circle
	{
		public $x;
		public $y;
		public $color;
	}

 	$imageWidth = $_GET['width'];
 	$imageHeight = $_GET['height'];
 	$circleRadius = $_GET['circle_radius'];
 	$isIntersected = (bool)$_GET['is_intersected'];
 	$circlesNb = $_GET['circles_nb'];
 	$impossibleToPlace = 0;

	$image = imagecreate($imageWidth, $imageHeight) or die("Cannot Initialize new GD image stream");
 	header('Content-Type: image/png');
 	$backgroundColor = imagecolorallocate($image, 255, 255, 255);


 	function checkForOverlap(&$curX, &$curY, $allCircles) {
 		global $circleRadius;
 		global $impossibleToPlace;


 		foreach ($allCircles as $currentCircle) {
 			$dx = abs($currentCircle->x - $curX);
 			$dy = abs($currentCircle->y - $curY);

 			$distBetwenCenters = sqrt($dx * $dx + $dy * $dy);
 			if ($distBetwenCenters < ($circleRadius * 2))
 			{
 				$curX = -1;
 				$curY = -1;
 				$impossibleToPlace++;
 				return false;
 			}
 		}
 		return true;
 	}

 	function calculateCircleCenter(&$circles) {
 		global $circleRadius;
 		global $imageWidth;
 		global $imageHeight;
 		global $isIntersected;
 		global $impossibleToPlace;

 		$curCircle = null;
 		$impossibleToPlace = 0;
	 	if($isIntersected == false)
	 	{
	 		$maxCirclesPlaced = ($imageWidth / ($circleRadius * 2)) * ($imageHeight / ($circleRadius * 2));
	 		for($i = 0; i < $maxCirclesPlaced; $i++)
	 		{
	 			$curX = rand($circleRadius, $imageWidth - $circleRadius);
	 			$curY = rand($circleRadius, $imageHeight - $circleRadius);
	 			if (checkForOverlap($curX, $curY, $circles) || $impossibleToPlace > 10)
	 				break;
	 		}
	 	}
	 	else {
	 		$curX = rand($circleRadius, $imageWidth - $circleRadius);
	 		$curY = rand($circleRadius, $imageHeight - $circleRadius);
	 	}
	 	if ($curX != -1)
	 	{
		 	$curCircle = new circle();
		 	$curCircle->x = $curX;
		 	$curCircle->y = $curY;
	 	}
	 	return $curCircle;
 	}

 	function calculateCircleColor() {
 		global $image;

 		$colorBlue = imagecolorallocate($image, 0, 0, 255);
 		$colorRed = imagecolorallocate($image, 255, 0, 0);
 		$colorGreen = imagecolorallocate($image, 0, 255, 0);
 		$colorBlack = imagecolorallocate($image, 0, 0, 0);
		$colorYellow = imagecolorallocate($image, 255, 255, 0);

		$allColors = array($colorBlue, $colorRed, $colorGreen, $colorBlack, $colorYellow);
		$randIndex = array_rand($allColors);
		$curCircleColor = $allColors[$randIndex];
	 	return ($curCircleColor);
 	}

 	function createCircle(&$circles) {
	 	$circle = calculateCircleCenter($circles);
	 	if ($circle == null)
	 		return;
	 	$circle->color = calculateCircleColor();

	 	array_push($circles, $circle);
 	}

 	$circles = array();
 	for($x = 0; $x < $circlesNb; $x++)
	 	createCircle($circles);
	foreach ($circles as $curCircle) {
	 	imagefilledellipse($image, $curCircle->x, $curCircle->y, $circleRadius * 2, $circleRadius * 2, $curCircle->color);
	}
	$imageSave = "./"."generated_image".".png";
	chmod($imageSave, 0755);
 	imagepng($image);
 	imagepng($image, $imageSave, 0, NULL);

 	imagedestroy($image);

?>